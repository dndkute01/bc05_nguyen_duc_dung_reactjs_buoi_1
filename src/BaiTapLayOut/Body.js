import React, { Component } from "react";

export default class extends Component {
  render() {
    return (
      <div>
        {/* Page Content*/}
        <section className="pt-4">
          <div className="container px-lg-5">
            {/* Page Features*/}
            <div className="row gx-lg-5">
              <div className="col-lg-4 col-xxl-3 mb-5">
                <div class="card bg-light border-0 h-100">
                  <div class="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                    <div class="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">
                      <i class="fab fa-battle-net "></i>
                    </div>
                    <h2 class="fs-4 fw-bold">Fresh new layout</h2>
                    <p class="mb-0">
                      With Bootstrap 5, we've created a fresh new layout for
                      this template!
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-xxl-3 mb-5">
                <div class="card bg-light border-0 h-100">
                  <div class="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                    <div class="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">
                      <i class="fab fa-battle-net "></i>
                    </div>
                    <h2 class="fs-4 fw-bold">Fresh new layout</h2>
                    <p class="mb-0">
                      With Bootstrap 5, we've created a fresh new layout for
                      this template!
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-xxl-3 mb-5">
                <div class="card bg-light border-0 h-100">
                  <div class="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                    <div class="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">
                      <i class="fab fa-battle-net "></i>
                    </div>
                    <h2 class="fs-4 fw-bold">Fresh new layout</h2>
                    <p class="mb-0">
                      With Bootstrap 5, we've created a fresh new layout for
                      this template!
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-xxl-3 mb-5">
                <div class="card bg-light border-0 h-100">
                  <div class="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                    <div class="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">
                      <i class="fab fa-battle-net "></i>
                    </div>
                    <h2 class="fs-4 fw-bold">Fresh new layout</h2>
                    <p class="mb-0">
                      With Bootstrap 5, we've created a fresh new layout for
                      this template!
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-xxl-3 mb-5">
                <div class="card bg-light border-0 h-100">
                  <div class="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                    <div class="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">
                      <i class="fab fa-battle-net "></i>
                    </div>
                    <h2 class="fs-4 fw-bold">Fresh new layout</h2>
                    <p class="mb-0">
                      With Bootstrap 5, we've created a fresh new layout for
                      this template!
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-xxl-3 mb-5">
                <div class="card bg-light border-0 h-100">
                  <div class="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                    <div class="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">
                      <i class="fab fa-battle-net "></i>
                    </div>
                    <h2 class="fs-4 fw-bold">Fresh new layout</h2>
                    <p class="mb-0">
                      With Bootstrap 5, we've created a fresh new layout for
                      this template!
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
