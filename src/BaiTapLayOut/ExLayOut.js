import React, { Component } from "react";
import Body from "./Body";
import Footer from "./Footer";
import Header from "./Header";
import NavBar from "./NavBar";

export default class ExLayOut extends Component {
  render() {
    return (
      <div>
        <NavBar />
        <Header />
        <Body />
        <Footer />
      </div>
    );
  }
}
