import logo from "./logo.svg";
import "./App.css";
import ExLayOut from "./BaiTapLayOut/ExLayOut";

function App() {
  return (
    <div className="App">
      <ExLayOut />
    </div>
  );
}

export default App;
